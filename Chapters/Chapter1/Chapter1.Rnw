%\setcounter{secnumdepth}{2}
% Chapter 1

\chapter{Introduction} % Main chapter title

\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

%----------------------------------------------------------------------------------------

\section {Background}
\label{sec:Backgr}

\doublespace

%Communicable diseases in human populations are a challenge to countries. Human interactions are the major causes of infectious disease spread which happens in households as well as in communities such as schools, workplaces and public areas. Hence understanding the household structure and human mobility pattern is important for disease surveillance, prevention and control. An outbreak of an infectious disease not only impacts the cost of healthcare but also the productivity and economy of the country. Also, there are hidden and long-term costs to the community after an outbreak such as loss of workforce. 

Human mobility refers to the movement of individuals or groups of individuals over different geographical and time scales \citep{Barbosa2018}. Human mobility is important for many applications such as infrastructure planning, traffic control and infectious disease control in order to identify and plan how many people will use related services, how people will be interacting with each other and how movement and contact of individuals will impact on disease transmission. Identifying the pattern of how people move in short, medium and long-term time scales 
%and how people come into contact at their households
is a challenging task due to difficulty in obtaining data \citep{Song2010}. If accurate mobility patterns could be predicted, 
%better control and prevention measures could be taken for infectious diseases.
policymakers can make better informed decisions on applications such as controlling an outbreak of disease.

There are two factors that make mobility difficult to understand. One factor is data on human movement is not readily available and often costly to obtain. The intention to travel varies based on individual need -- for job, education, family relationship, entertainment, health or any other general activity. Multiple data sources such as mobile phone data, census data, social media data, travel details or currency note tracking are used in order to identify mobility pattern which capture different intentions to travel. But all these data sets have their own limitations and selecting one of them will not accurately interpret the travel pattern of the population. For example, if mobile phone data is selected, not everyone in a population would own a mobile phone \citep{Rennie2013} and if census data is selected, in Australia, it partially captures the movement pattern for selected intentions such as journey to work. 

The second factor is that the data sources which are available are not complete in most situations with missing data. Specifically, rural regions have very sparse data on human mobility. Some longitudinal studies \citep{Sayers2003} collect location as part of the study which can be used to identify mobility, however, data collected with the objective of identifying mobility is sparse and costly particularly in rural regions. %The third factor is the lack of existing models to understand and characterise human mobility as per the author's knowledge, especially in rural regions. %such as in Indigenous communities in Australia.

%Especially, low income earning communities in rural regions are at risk of getting affected by infectious diseases and could have a wider spread due to lack of health facilities and awareness. Human contact which happens at household and community level is one of the major causes of infectious disease transmission. Identification of the differences in household structures in rural communities gives an insight on whether this has any impact on disease transmission and also how the household structure differences across regions impact mobility related to family connections \citep{Vino2017}. Therefore, it is of immense importance to 

%Various mechanistic and statistical human mobility models are used in past literature in an attempt to predict human mobility patterns. Mechanistic models includes the Gravity model \citep{Flowerdew1982} and the Radiation model \citep{Simini2012} which are widely used in modelling human mobility patterns. Other statistical models are STEPS-Spatio-TEmporal Parametric Stepping \citep{Nguyen2011a}, continuous-time random-walk (CTRW) model \citep{Brockmann2006} and Truncated Levy Walk mobility (TLW) model \citep{Rhee2011}. Each of these models have their advantages, limitations and requirement of data which are discussed in the next chapter.
 % Models help to simulate and better understand the real world systems \citep{Ford2009}.% Identifying a mobility model to understand movement in rural regions is more challenging as it generally also lacks real-time data on mobility patterns. 
Models are useful in simulating and understanding the real world systems \citep{Ford2009}. Mobility models have been used to understand and predict movement or travel behaviour of species, and humans \citep{Carey1858,Milne2008,Simini2012,Ashworth2017}, movement of particle compounds \citep{Ellmer2001} and in mobile and cellular device simulations \citep{Klaassen1992,Camp2002}. Two main classes of model that are used to describe human mobility are population level and individual level models depending on whether they predict mobility patterns at the population or individual level.
%Two main classes of model that are used to describe mobility are mechanistic and statistical models. Mechanistic models are mathematical formulations based on assumptions on the underlying mechanisms of real world \citep{Baker2018} and statistical models use statistics to predict and make inference on data.% are used in past literature in an attempt to predict human mobility patterns.
%Mechanistic models includes models such as the gravity model \citep{Flowerdew1982} and the radiation model \citep{Simini2012} which are widely used in modelling human mobility patterns. Some examples of other statistical models used in mobility modelling are STEPS-Spatio-TEmporal Parametric Stepping \citep{Nguyen2011a}, continuous-time random-walk (CTRW) model \citep{Brockmann2006} and Truncated Levy Walk mobility (TLW) model \citep{Rhee2011}. 
Key examples of these models, their advantages, limitations and requirement of data are discussed in chapter 2.

%The study conducted in Sub-Saharan Africa on regional mobility by \citet{Wesolowski2015} also mention that the community structure in rural areas is different. Compared to high-income areas, accessibility, transport availability, cost of travel could vary substantially and even the intention for travel could vary among these regions. %Targeting the mega-cities, these rural regions could have informal labour movements rather than regular commuting patterns.

In this thesis I address these challenges, using remote Indigenous communities of Northern Territory, Australia as a case study. Longitudinal data, including mobility data, was collected from this region as part of two previous studies Aboriginal Birth Cohort Study \citep{Sayers2003} and NintiOne \citep{Dockery2012} data set and Australian census data are used which are in different time scales, with different intentions to travel and with missing data. Utilising the existing resources to understand mobility for regions like Northern Territory would not only help public health agencies to make decisions on control measures but would also contribute to other policy making decisions such as infrastructure development and economic planning. The study by \citet{Mangal2016} has used human mobility models to forecast the districts which have a high risk of polio virus transmission in Nigeria so that planning and policy decisions such as the feasibility of stopping the outbreak by immunising only in neighbouring districts could be made. This study relies on assumptions of mechanistic model rather than empirical data. Thus, understanding mobility for rural area would contribute in applications such as prevention and management of infectious diseases.

In this study I focus on the implications of the findings of rural mobility from an infectious disease point of view. Indigenous communities tend to have a higher burden of infectious disease compared to non-Indigenous communities \citep{Flint2010,Trauer2011}. One of the major challenges faced by clinicians and policy makers is that once a control program is implemented on a community it gets reinfected within a short period of time. Also, there are different strains in these communities (e.g. Group A Streptococci, \citet{Valery2008}) . Therefore, researchers are interested in understanding whether mobility plays a major role in the reinfection and understand the mobility in rural Indigenous communities.

%In addition, detailed household-level information is often not publicly available in most demographic data collection surveys including the national census. This is particularly the case in resource-limited settings where literacy levels may be low and household structures may differ markedly from the nuclear household structure typically assumed by survey designs \citep{Morphy2006}. For example, Indigenous households in Australia tend to be larger than non-Indigenous households, contain more extended family members, and may change in composition more rapidly \citep{Morphy2006,Morphy2007}. Furthermore, national censuses are resource intensive and conducted relatively infrequently. There is therefore a need for more lightweight methods that allow for rapid, repeated measurement in specific populations where literacy levels may be low.

%The studies considering rural regions \citep{Mangal2016,Wesolowski2015} have identified the Radiation model as the closest fit compared to gravity and simple distance model. However, both the studies have identified that using the Radiation model is not the best solution and \citet{Wesolowski2015} states that by using these models, there is a chance of misrepresentation of population dynamics for the infectious disease spread.

As per our knowledge, there is no existing data-based methods to understand the human mobility patterns in rural areas while incorporating the challenges of imperfect empirical data on the mobility pattern. This is a challenge which needs to be addressed in order to understand human mobility in rural regions.

%\subsection {Understanding human mobility}
%\subsection {Addressing imperfect data}
%\subsection {Combining multiple sources of data}

\section {Aims and objectives}

Given the challenges associated with collecting mobility data in rural areas, it is desirable to make best use of existing mobility data. The aim of this project was to evaluate a range of statistical techniques for identifying factors associated with human mobility %Human mobility models identified as best suited for rural regions incorporating the limitations of missing data and multiple sources of data would contribute to creating new knowledge on more accurate prediction of human mobility in these regions. %Further, the methodological approach in understanding household structure in rural areas and the statistical techniques used for better prediction of mobility with data limitations could be applied for other rural regions in the world.


%\subsection {Motivation to understand household structure and mobility pattern in Australian Indigenous communities}

%\section {Objectives}

The specific objectives of this project were to:

\begin {itemize}

\item
Objective 1: Review existing mobility models, assess the suitability of existing models to characterize rural human mobility with existing data, and outline the gap in modelling rural human mobility their applicability to a rural region;

%\item
%Objective 2: Understand the household structure in rural/urban Indigenous populations, estimate close contact and model the implication on disease transmission

%\item 
%Objective 3: Identification of existing methods to integrate multiple scales and multiple sources of data

\item 
Objective 2: Demonstrate that in situations where there is hierarchical data, the GLMMs are more suitable than the existing mobility models for capturing the within and in between-group variations of human movement;

\item
Objective 3: Assess how different approaches to addressing missingness affect the estimates made by predictive models, and evaluate which approaches may be suited to the characteristics of data on rural mobility; and

\item
Objective 4: Evaluate techniques for combining multiple sources of data to make inferences using additional variables from multiple data sets to understand rural mobility which cannot be obtained when using a single data source

\end {itemize}

\section {Structure of the thesis}

%In this thesis, I look at the existing methods of modelling human mobility and aim to develop a methodology to address imperfect data from multiple sources and show the importance/ implications of ignoring them when making inferences on human movement. Human mobility in rural areas, especially in Indigenous Australians, is taken as a case study to show this impact.

%write a sentence for each Chapter describing the specific aim of that Chapter
%In order to XX I need YY; this Chapter introduces <method ZZ> and applies it to AA and BB showing that CC

In this thesis I evaluate the existing modelling approaches, statistical techniques and their usefulness for addressing limitations of mobility data. Statistical analysis of the constraints and approaches of dealing with imperfect data, identification of the ideal outcome with perfect data and information on the drawbacks of imperfect data will help the policy makers in future fund and resource allocation.

The thesis structure is as follows:\\
In Chapter 2, I review the existing individual and population level
%statistical and mechanistic 
approaches of modelling movement. The models are assessed for their suitability to characterize rural human mobility with existing data and the current gap in literature for modelling human mobility in rural areas are discussed. 

In Chapter 3, I give an overview of the Indigenous population which is used as a case study in this project and apply the existing models -- gravity and radiation, to predict future human mobility at population level in rural regions and identify the gaps of relying only on these models. %Also, we aim to show the importance of understanding human mobility by applying it to a real world scenario. 
%In this chapter, we discuss the data sources and give a descriptive analysis of the short/medium- and long-term mobility patterns. The relevance of the existing models to the data sets are evaluated. The movement network is mapped and the popular gravity and radiation models are applied to the data sets to understand the mobility patterns. The implications of contact in terms of an infectious disease transmission is also discussed at household contact and community contact levels. For household contact a contact matrix is derived and for community contact, a metapopulation model (?) is applied and a simple SEIR model is applied to show the implications.

In Chapter 4, I evaluate a hierarchical modelling approach to understand and identify factors influencing human mobility in rural regions. I propose a methodology for making inference on the hierarchical longitudinal data set by applying generalised linear mixed models (GLMM). I discuss the applicability of the techniques for estimation of parameters and model selection and model the long- and medium-term mobility in Indigenous communities.

The two data sets used in Chapter 4 both contain substantial missing data values. Chapter 5 addresses the challenge of imperfect data. I discuss and apply statistical techniques to address missingness. I then evaluate them by comparing results to those obtained using only the complete data set in Chapter 4.

Chapters 4 and 5 each analysed the two mobility data sets independently. In Chapter 6 I investigate the possibility of integrating these two data sources to determine whether data can be appropriately combined to make inferences using additional variables to understand rural mobility. I review methodologies of integrating multiple data sources, apply identified technique to two different data sources (ABC and NintiOne) and make inference on rural mobility on the combined data set.

Chapter 7 summarises the results reported in preceding chapters and discusses %look at the overall methodology of how to deal with multiple sources of imperfect data. We take human mobility in rural areas as a case study in this chapter. We combine the methodologies in chapter 4, 5 and apply this to the integrated data set in Chapter 6. We also
how each of the investigated techniques can best be applied to improve understanding of human mobility in rural regions, the limitations of the project and future research needs.% the implications/challenges in the techniques of the project to understand human mobility in rural regions.
%inference by ignoring imperfect data and combining multiple sources of data.
